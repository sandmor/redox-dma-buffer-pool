extern crate alloc;

use alloc::collections::BTreeMap;
use alloc::sync::Arc;
use core::marker::PhantomData;
use core::mem;
use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;
use lazy_static::lazy_static;
use parking_lot::Mutex;
use redox_buffer_pool::{AllocationStrategy, BufferPool, BufferSlice, Handle, MmapEntries};
use syscall::error::{Error, Result, ENOMEM};
use syscall::io::{Dma, PhysBox};

pub use redox_buffer_pool::{Guard, Integer, NoGuard, ReclaimError, WithGuardError};

const PAGE_SIZE: usize = 4096;
const DEFAULT_EXPAND_SIZE: usize = PAGE_SIZE;
const DEFAULT_CAPACITY: usize = PAGE_SIZE;

lazy_static! {
    static ref DMAS: Mutex<DmaStore> = Mutex::new(DmaStore(BTreeMap::new()));
}

struct DmaStore(BTreeMap<usize, Dma<[u8]>>);

unsafe impl Send for DmaStore {}
unsafe impl Sync for DmaStore {}

fn alloc(size: usize) -> Result<Dma<[u8]>> {
    let phys = PhysBox::new(size)?;
    Ok(unsafe { Dma::from_physbox_uninit_unsized(phys, size)?.assume_init() })
}

fn alloc_32(size: usize) -> Result<Dma<[u8]>> {
    let phys = PhysBox::new_in_32bit_space(size)?;
    Ok(unsafe { Dma::from_physbox_uninit_unsized(phys, size)?.assume_init() })
}

pub struct DmaBufferPool {
    mngr: BufferPool<usize, DmaHandle, usize>,
    in_32bit_space: bool,
}

impl DmaBufferPool {
    pub fn new(in_32bit_space: bool) -> Result<Self> {
        Self::with_capacity(in_32bit_space, DEFAULT_CAPACITY)
    }

    /// Create a new empty DMA buffer pool with the specified capacity.
    pub fn with_capacity(in_32bit_space: bool, capacity: usize) -> Result<Self> {
        let mngr = BufferPool::new(Some(DmaHandle));
        let pool = Self {
            mngr,
            in_32bit_space,
        };
        pool.reserve(capacity)?;
        Ok(pool)
    }

    /// Reserves capacity for exactly `additional` more bytes
    pub fn reserve_exact(&self, additional: usize) -> Result<()> {
        let mut dma = match self.in_32bit_space {
            true => alloc_32(additional)?,
            false => alloc(additional)?,
        };
        let phys_addr = dma.physical();
        let ptr = unsafe { NonNull::new_unchecked(dma.as_mut_ptr()) };
        DMAS.lock().0.insert(phys_addr, dma);
        let handle = match self.mngr.begin_expand(additional) {
            Ok(handle) => handle,
            Err(_) => {
                return Err(Error::new(ENOMEM));
            }
        };
        unsafe {
            handle.initialize(ptr, phys_addr);
        }
        Ok(())
    }

    /// Reserves capacity at least for `additional` more bytes
    pub fn reserve(&self, mut additional: usize) -> Result<usize> {
        additional = additional + PAGE_SIZE - additional % PAGE_SIZE;
        self.reserve_exact(additional)?; // Round up `additional` to page size
        Ok(additional)
    }

    pub fn shared(self) -> Arc<Self> {
        Arc::new(self)
    }

    /// Try to acquire a statically (as in compiler-checked and lifetime-tied) borrowed slice, from
    /// this buffer. The slice will automatically be reclaimed upon drop, so long as there is no
    /// guard protecting the slice at that time. If there is, the memory will be leaked instead,
    /// and the pool will not be able to use the offset, as it will be marked "occpied" and nothing
    /// will free it.
    pub fn acquire_borrowed_slice<G: Guard>(
        &self,
        len: usize,
        alignment: usize,
        strategy: DmaAllocationStrategy,
    ) -> Result<DmaBufferSlice<'_, G>> {
        match self
            .mngr
            .acquire_borrowed_slice(len, alignment, strategy.into())
        {
            Some(slice) => Ok(DmaBufferSlice { slice }),
            None => {
                self.reserve(DEFAULT_EXPAND_SIZE.max(alignment.max(1) - 1 + len))?;
                match self
                    .mngr
                    .acquire_borrowed_slice(len, alignment, strategy.into())
                {
                    Some(slice) => Ok(DmaBufferSlice { slice }),
                    None => Err(Error::new(ENOMEM)),
                }
            }
        }
    }

    /// Try to acquire a statically (as in compiler-checked and lifetime-tied) borrowed slice, from
    /// this buffer. The slice will automatically be reclaimed upon drop, so long as there is no
    /// guard protecting the slice at that time. If there is, the memory will be leaked instead,
    /// and the pool will not be able to use the offset, as it will be marked "occpied" and nothing
    /// will free it.
    pub fn acquire_borrowed_object<O: Sized, G: Guard>(
        &self,
        obj: O,
        alignment: usize,
        strategy: DmaAllocationStrategy,
    ) -> Result<DmaObject<'_, O, G>> {
        self.acquire_borrowed_slice(mem::size_of::<O>(), alignment, strategy)
            .map(|slice| DmaObject::new(slice, obj))
    }

    // TODO: Expose acquire_weak_slice and acquire_strong_slice methods
}

pub struct DmaBufferSlice<'a, G: Guard> {
    slice: BufferSlice<'a, usize, DmaHandle, usize, G>,
}

impl<'a, G> DmaBufferSlice<'a, G>
where
    G: Guard,
{
    /// Checks whether the pool that owns this slice is still alive, or if it has dropped. Note
    /// that this only makes sense for weak buffer slices, since buffer slices tied to a lifetime
    /// cannot outlive their pools (checked for at compile time), while strong buffer slices ensure
    /// at runtime that they outlive their pools.
    ///
    /// For weak buffer slices, this method should be called before doing anything with the slice,
    /// since a single deref could make it panic if the buffer isn't there anymore.
    pub fn pool_is_alive(&self) -> bool {
        self.slice.pool_is_alive()
    }

    /// Construct an immutable slice from this buffer.
    ///
    /// # Panics
    ///
    /// This method will panic if this is a weak slice, and the buffer pool has been destroyed..
    pub fn as_slice(&self) -> &[u8] {
        self.slice.as_slice()
    }
    /// Tries to construct an immutable slice from this buffer, returning None if the pool has been
    /// dropped (and hence, this is a weak slice).
    pub fn try_as_slice(&self) -> Option<&[u8]> {
        self.slice.try_as_slice()
    }
    /// Construct a mutable slice from this buffer.
    ///
    /// # Panics
    ///
    /// Like [`as_slice`], this method will panic if the buffer pool has been destroyed.
    ///
    /// [`as_slice`]: #method.as_slice
    pub fn as_slice_mut(&mut self) -> &mut [u8] {
        self.slice.as_slice_mut()
    }

    /// Tries to construct a mutable slice from this buffer, returning None if the pool has been
    /// destroyed.
    pub fn try_as_slice_mut(&mut self) -> Option<&mut [u8]> {
        self.slice.try_as_slice_mut()
    }

    /// Forcefully remove a guard from a future, from this slice, returning it if there was a guard
    /// already.
    ///
    /// # Safety
    /// This is unsafe because it allows removing guards set by pending futures; although this is
    /// completely fine when there are no pending ones, the buffer slice will be reclaimed without
    /// the guard, causing UB if any producer keeps using its pointer.
    pub unsafe fn unguard(&mut self) -> Option<G> {
        self.slice.unguard()
    }

    /// Adds a guard to this buffer, preventing it from deallocating unless the guard accepts that.
    /// This is crucial when memory is shared with another component that may be outside this
    /// process's address space. If there is a pending io_uring submission or a pending NVME
    /// command for instance, this guard will fail if the buffer is in use by a command, and leak
    /// the memory instead when dropping.
    ///
    /// This will error with [`WithGuardError`] if there is already an active guard.
    pub fn guard(&mut self, guard: G) -> Result<(), WithGuardError<G>> {
        self.slice.guard(guard)
    }

    /// Tries to add a guard of potentially a different type than the guard type in this slice.
    /// Because of that this, this will consume self and construct a different `DmaBufferSlice`
    /// with a different guard type, or error with `self` if there was already a guard present.
    pub fn with_guard<OtherGuard: Guard>(
        self,
        other: OtherGuard,
    ) -> Result<DmaBufferSlice<'a, OtherGuard>, WithGuardError<Self>> {
        self.slice
            .with_guard(other)
            .map(|slice| DmaBufferSlice { slice })
            .map_err(|error| WithGuardError {
                this: DmaBufferSlice { slice: error.this },
            })
    }

    /// Reclaim the buffer slice, equivalent to dropping but with a Result. If the buffer slice was
    /// guarded by a future, this will fail with [`ReclaimError`] if the future hadn't completed when
    /// this was called.
    pub fn reclaim(self) -> Result<(), ReclaimError<Self>> {
        self.slice.reclaim().map_err(|error| ReclaimError {
            this: DmaBufferSlice { slice: error.this },
        })
    }

    /// Get the offset of the buffer pool where this was allocated.
    pub fn offset(&self) -> usize {
        self.slice.offset()
    }

    /// Get the length of the allocation slice.
    pub fn len(&self) -> usize {
        self.slice.len()
    }

    /// Get the capacity of the allocation slice. This is almost always the same as the length, but
    /// may be larger in case the allocator chose a larger size to align the range afterwards.
    pub fn capacity(&self) -> usize {
        self.slice.capacity()
    }

    /// Get the physical address of the allocation slice.
    pub fn physical(&self) -> usize {
        self.slice.extra() + self.slice.mmap_offset()
    }
}

pub struct DmaObject<'a, O: Sized, G: Guard> {
    slice: DmaBufferSlice<'a, G>,
    phantom: PhantomData<O>,
}

impl<'a, O, G> DmaObject<'a, O, G>
where
    O: Sized,
    G: Guard,
{
    pub fn new(mut slice: DmaBufferSlice<'a, G>, obj: O) -> DmaObject<'a, O, G> {
        assert!(mem::size_of::<O>() >= slice.len());
        unsafe {
            *(slice.as_slice_mut().as_mut_ptr() as *mut O) = obj;
        }
        DmaObject {
            slice,
            phantom: PhantomData,
        }
    }

    /// Checks whether the pool that owns this object is still alive, or if it has dropped. Note
    /// that this only makes sense for weak buffer objects, since buffer objects tied to a lifetime
    /// cannot outlive their pools (checked for at compile time), while strong buffer objects ensure
    /// at runtime that they outlive their pools.
    ///
    /// For weak buffer objects, this method should be called before doing anything with the object,
    /// since a single deref could make it panic if the buffer isn't there anymore.
    pub fn pool_is_alive(&self) -> bool {
        self.slice.pool_is_alive()
    }

    /// Construct an immutable slice from this object.
    ///
    /// # Panics
    ///
    /// This method will panic if this is a weak object, and the buffer pool has been destroyed..
    pub fn as_slice(&self) -> &[u8] {
        self.slice.as_slice()
    }
    /// Tries to construct an immutable slice from this object, returning None if the pool has been
    /// dropped (and hence, this is a weak object).
    pub fn try_as_slice(&self) -> Option<&[u8]> {
        self.slice.try_as_slice()
    }
    /// Construct a mutable slice from this object.
    ///
    /// # Panics
    ///
    /// Like [`as_slice`], this method will panic if the buffer pool has been destroyed.
    ///
    /// [`as_slice`]: #method.as_slice
    pub unsafe fn as_slice_mut(&mut self) -> &mut [u8] {
        self.slice.as_slice_mut()
    }

    /// Tries to construct a mutable slice from this object, returning None if the pool has been
    /// destroyed.
    pub unsafe fn try_as_slice_mut(&mut self) -> Option<&mut [u8]> {
        self.slice.try_as_slice_mut()
    }

    /// Forcefully remove a guard from a future, from this slice, returning it if there was a guard
    /// already.
    ///
    /// # Safety
    /// This is unsafe because it allows removing guards set by pending futures; although this is
    /// completely fine when there are no pending ones, the buffer slice will be reclaimed without
    /// the guard, causing UB if any producer keeps using its pointer.
    pub unsafe fn unguard(&mut self) -> Option<G> {
        self.slice.unguard()
    }

    /// Adds a guard to this object buffer, preventing it from deallocating unless the guard accepts that.
    /// This is crucial when memory is shared with another component that may be outside this
    /// process's address space. If there is a pending io_uring submission or a pending NVME
    /// command for instance, this guard will fail if the buffer is in use by a command, and leak
    /// the memory instead when dropping.
    ///
    /// This will error with [`WithGuardError`] if there is already an active guard.
    pub fn guard(&mut self, guard: G) -> Result<(), WithGuardError<G>> {
        self.slice.guard(guard)
    }

    /// Tries to add a guard of potentially a different type than the guard type in this object.
    /// Because of that this, this will consume self and construct a different `DmaObject`
    /// with a different guard type, or error with `self` if there was already a guard present.
    pub fn with_guard<OtherGuard: Guard>(
        self,
        other: OtherGuard,
    ) -> Result<DmaObject<'a, O, OtherGuard>, WithGuardError<Self>> {
        self.slice
            .with_guard(other)
            .map(|slice| DmaObject {
                slice,
                phantom: PhantomData,
            })
            .map_err(|error| WithGuardError {
                this: DmaObject {
                    slice: error.this,
                    phantom: PhantomData,
                },
            })
    }

    /// Reclaim the buffer slice, equivalent to dropping but with a Result. If the buffer slice was
    /// guarded by a future, this will fail with [`ReclaimError`] if the future hadn't completed when
    /// this was called.
    pub fn reclaim(self) -> Result<(), ReclaimError<Self>> {
        self.slice.reclaim().map_err(|error| ReclaimError {
            this: DmaObject {
                slice: error.this,
                phantom: PhantomData,
            },
        })
    }

    /// Get the offset of the buffer pool where this was allocated.
    pub fn offset(&self) -> usize {
        self.slice.offset()
    }

    /// Get the length of the allocation slice where this object is store.
    pub fn len(&self) -> usize {
        self.slice.len()
    }

    /// Get the capacity of the allocation slice. This is almost always the same as the length, but
    /// may be larger in case the allocator chose a larger size to align the range afterwards.
    pub fn capacity(&self) -> usize {
        self.slice.capacity()
    }

    pub fn physical(&self) -> usize {
        self.slice.physical()
    }
}

impl<'a, O, G> Deref for DmaObject<'a, O, G>
where
    O: Sized,
    G: Guard,
{
    type Target = O;

    fn deref(&self) -> &Self::Target {
        unsafe { &*(self.slice.as_slice().as_ptr() as *const Self::Target) }
    }
}

impl<'a, O, G> DerefMut for DmaObject<'a, O, G>
where
    O: Sized,
    G: Guard,
{
    fn deref_mut(&mut self) -> &mut O {
        unsafe { &mut *(self.slice.as_slice_mut().as_mut_ptr() as *mut O) }
    }
}

/// The strategy to use when allocating, with tradeoffs between heap fragmentation, and the
/// algorithmic complexity of allocating.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum DmaAllocationStrategy {
    /// Allocate as optimally as possible, by taking some extra time into making sure that the
    /// smallest range with the smallest alignment possible is used. This is currently O(1) in best
    /// case, but O(n) in worst case.
    Optimal,

    /// Allocate as quickly as possible. This is currently implemented as a B-tree lookup in the
    /// free space map, so it'll prioritize smaller ranges, and smaller alignments afterwards. This
    /// is O(log n) in best case, average case, and worst case.
    Greedy,

    /// Allocate at a fixed offset. Since this won't cause the allocator to find a suitable range,
    /// but only check whether the range requested exists, this is also O(log n) in best case,
    /// average case, and worst case.
    Fixed(usize),
}

impl Default for DmaAllocationStrategy {
    fn default() -> Self {
        Self::Optimal
    }
}

impl Into<AllocationStrategy<usize>> for DmaAllocationStrategy {
    fn into(self) -> AllocationStrategy<usize> {
        match self {
            DmaAllocationStrategy::Optimal => AllocationStrategy::Optimal,
            DmaAllocationStrategy::Greedy => AllocationStrategy::Greedy,
            DmaAllocationStrategy::Fixed(v) => AllocationStrategy::Fixed(v),
        }
    }
}

#[derive(Debug)]
struct DmaHandle;

impl Handle<usize, usize> for DmaHandle {
    type Error = ();
    fn close(&mut self, mmap_entries: MmapEntries<usize, usize>) -> Result<(), Self::Error> {
        let mut dmas_repo = DMAS.lock();
        mmap_entries.for_each(|entry| {
            dmas_repo.0.remove(&entry.extra);
        });
        Ok(())
    }
}
